import { LitElement, html } from 'lit-element';
class PersonaStats extends LitElement {

    static get properties() {
      return {
        people: {
          type: Array
        }
      }
    }

    constructor() {
      super();

      this.people = []
    }

    updated(changedProperties){
      console.log("updated de persona-stats")
      this.dispatchEvent(new CustomEvent("updated-stats", {
        detail: {
          stats: {
            sum: this.peopleCount(),
            max: this.maxYearsInCompany()
          }
        }
      }))
    }


  render() {
    return "";
  }

  peopleCount(){
    return this.people.length
  }

  maxYearsInCompany(){
    if (this.peopleCount() === 0) {
      return 0
    }
    let years = this.people.map( person => person.yearsInCompany)
    return Math.max(...years)
  }
}

customElements.define('persona-stats', PersonaStats)
