import {
  LitElement,
  html
} from 'lit-element';

class FichaPersona extends LitElement {

  static get properties() {
    return {
      name: {
        type: String
      },
      yearsInCompany: {
        type: Number
      },
      personInfo: {
        type: String
      },
      photo: {
        type: Object
      }
    };

  }


  constructor() {
    super();

    this.name = "Prueba nombre"
    this.yearsInCompany = 12
    this.photo = {
      src: "./img/profile.jpg",
      alt: "foto persona"
    }

    this.updatePersonInfo();

  };


  updated(changedProperties) {
    changedProperties.forEach((oldValue, propName) => {
      console.log("Propiedad " + propName + " cambia de valor, antiguo " + oldValue)
    })

    if (changedProperties.has("name")) {
      console.log("Propiedad name cambia valor anterior era " +
        changedProperties.get("name") + " nuevo es " + this.name)
    }

    if (changedProperties.has("yearsInCompany")) {
      console.log("Propiedad name cambia valor anterior era " +
        changedProperties.get("name") + " nuevo es " + this.name)
        this.updatePersonInfo();
    }
  }

  render() {
    return html `
      <div>
         <label>Nombre completo</label>
         <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
         <br/>
          <label>Años en la empresa</label>
          <input type="text" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
          <br/>
           <label>Años en la empresa</label>
           <input type="text" value="${this.personInfo}" disabled></input>
           <br/>
          <img src="${this.photo.src}" height="100" width="100" alt="${this.photo.alt}" />
      </div>
      `;
  }
  updateName(e) {
    console.log("updateName")
    console.log("Actualizando propiedad nombre con el valor " + e.target.value)
    this.name = e.target.value;
  }
  updateYearsInCompany(e) {
    console.log("updateYearsInCompany")
    console.log("Actualizando propiedad years in company con el valor " + e.target.value)
    this.yearsInCompany = e.target.value;
  }


    updatePersonInfo() {
      console.log("updatePersonInfo")

      if (this.yearsInCompany >= 7){
        this.personInfo = "lead"
      } else if (this.yearsInCompany >= 5){
        this.personInfo = "senior"
      }  else if (this.yearsInCompany >= 3){
        this.personInfo = "team"
      } else {
        this.personInfo = "junior"
      }
    }

}

customElements.define('ficha-persona', FichaPersona)
