import { LitElement, html } from 'lit-element';

class PersonaMainDm extends LitElement {
  static get properties() {
      return {
        people: {
          type: Array
        }
      }
    }

    constructor() {
      super();

      this.people = [{
        name: "Ellen Ripley",
        yearsInCompany: 10,
        profile: "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
        photo: {
          src: "./img/profile.jpg",
          alt: "Ellen Ripley"
        }
      }, {
        name: "Bruce Banner",
        yearsInCompany: 2,
        profile: "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
        photo: {
          src: "./img/profile.jpg",
          alt: "Bruce Banner"
        }
      }, {
        name: "Eowin",
        yearsInCompany: 5,
        profile: "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
        photo: {
          src: "./img/profile.jpg",
          alt: "Eowin"
        }
      }, {
        name: "Turanga Leela",
        yearsInCompany: 9,
        profile: "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ",
        photo: {
          src: "./img/profile.jpg",
          alt: "Turanga Leela"
        }
      }]

      this.showPersonForm = false;
    }

    updated(changedProperties) {
      console.log("updated");
      if (changedProperties.has("people")) {
          console.log("ha cambiado el valor de la propiedad people");

          this.dispatchEvent(new CustomEvent("people-data-updated", {
            detail: {
              people: this.people
            }
          }))
        }

      }

}

customElements.define('persona-main-dm', PersonaMainDm)
