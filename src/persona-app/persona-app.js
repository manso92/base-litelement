import { LitElement, html } from 'lit-element';
import '../persona-header/persona-header.js'
import '../persona-main/persona-main.js'
import '../persona-footer/persona-footer.js'
import '../persona-stats/persona-stats.js'
import '../persona-sidebar/persona-sidebar.js'

class PersonaApp extends LitElement {
  render() {
    return html`

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <persona-header></persona-header>

    <div class="row">
        <persona-sidebar class="col-2" @updated-filter="${this.updateFilter}" @new-person="${this.newPerson}"></persona-sidebar>
        <persona-main class="col-10" @updated-people="${this.updatedPeople}"></persona-main>
    </div>
    <persona-footer></persona-footer>
    <persona-stats @updated-stats="${this.updatedStats}"></persona-stats>
  `;
  }

  newPerson(e) {
    console.log("newPerson en persona.app")
    this.shadowRoot.querySelector("persona-main").showPersonForm = true;
  }

  updatedPeople(e){
    console.log("updatedPeople en persona-app")
    this.shadowRoot.querySelector("persona-stats").people = e.detail.people;
  }

  updatedStats(e){
    console.log("updatedStats en persona-app")
    this.shadowRoot.querySelector("persona-sidebar").peopleCount = e.detail.stats.sum;
    this.shadowRoot.querySelector("persona-sidebar").maxYearFilter = e.detail.stats.max;
  }

  updateFilter(e){
      console.log("updateFilter en persona.app")
      this.shadowRoot.querySelector("persona-main").yearsFilter = e.detail.yearsFilter;
  }
}

customElements.define('persona-app', PersonaApp)
