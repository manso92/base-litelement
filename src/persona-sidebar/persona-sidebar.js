import {
  LitElement,
  html
} from 'lit-element';

class PersonaSidebar extends LitElement {

  static get properties() {
    return {
      peopleCount: {
        type: Number
      },
      yearsFilter: {
        type: Number
      },
      maxYearFilter: {
        type: Number
      }
    }
  }

  constructor() {
    super();

    this.peopleCount = 0
    this.yearsFilter = 100
    this.maxYearFilter = 100
  }

  updated(changedValues){
      if (changedValues.has("yearsFilter")) {
        this.dispatchEvent(new CustomEvent("updated-filter", {
          detail: {
            yearsFilter: this.yearsFilter
          }
        }))
      }
      if (changedValues.has("maxYearFilter")) {
        if(changedValues.get("maxYearFilter") === this.yearsFilter){
          this.yearsFilter = this.maxYearFilter
        }
        if(this.yearsFilter > this.maxYearFilter){
          this.yearsFilter = this.maxYearFilter
        }
      }
  }

  render() {
    return html `
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

      <aside class="col-10">
         <section>
             <div class="mt-5">
                 <button class="w-100 btn btn-success" stype="font-size: 50px" @click="${this.newPerson}"><strong>+</strong></button>
             </div>
             <div class="mt-5">
                 Actualmente hay ${this.peopleCount} personas registradas
             </div>
             <div class="mt-5">
                 <!-- TODO hacer variable el máximo -->
                 Actualmente se están filtrando las personas con años en la empresa superiores a ${this.yearsFilter}
                 <input type="range" class="custom-range" min="0" max="${this.maxYearFilter}"
                        value="${this.yearsFilter}" step="1" @change="${this.updateFilter}" />
             </div>
         </section>
      </aside>
  `;
  }

  updateFilter(e) {
    console.log("Filtro actualizado")
    this.yearsFilter = e.target.value
  }

  newPerson(e) {
    console.log("newPerson en persona-sidebar")
    console.log("Se va a crear una persona nueva")

    this.dispatchEvent(new CustomEvent("new-person", {}));
  }
}

customElements.define('persona-sidebar', PersonaSidebar)
