import {
  LitElement,
  html,
  css
} from 'lit-element';

import '../persona-ficha-listado/persona-ficha-listado.js'
import '../persona-form/persona-form.js'
import '../persona-main-dm/persona-main-dm.js'

class PersonaMain extends LitElement {

  static get properties() {
    return {
      people: {
        type: Array
      },
      showPersonForm: Boolean,
      yearsFilter: Number
    }
  }

  static get styles(){
    return css`
      :host{
          all: initial;
      }
    `
  }

  constructor() {
    super();

    this.people = []
    this.showPersonForm = false;
  }

  updated(changedProperties) {
    console.log("updated de persona-main")

    if (changedProperties.has("showPersonForm")) {
      if (this.showPersonForm === true) {
        this.showPersonFormData()
      } else {
        this.showPersonList()
      }

    }

    if (changedProperties.has("people")) {
      console.log("Ha cambiado el valor de la propiedad people en persona-main");
      this.dispatchEvent(new CustomEvent("updated-people", {
        detail: {
          people: this.people
        }
      }))
    }
  }

  render() {
    return html `
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
      <h2 class="text-center">Personas</h2>

      <div class="row" id="peopleList">
        <div class="row row-cols-1 row-cols-sm-4">
          ${this.people.filter(
            person => person.yearsInCompany <= this.yearsFilter
          ).map(
            person => html `
                <persona-ficha-listado
                    fname="${person.name}"
                    yearsInCompany="${person.yearsInCompany}"
                    profile="${person.profile}"
                    .photo="${person.photo}"
                    @delete-person="${this.deletePerson}"
                    @info-person="${this.infoPerson}">
                </persona-ficha-listado>
            `
          )}
        </div>
      </div>
      <div class="row">
         <persona-form id="personForm" class="d-none"
                       @persona-form-close="${this.personFormClose}"
                       @persona-form-store="${this.personFormStore}">
         </persona-form>
      </div>
      <persona-main-dm @people-data-updated="${this.peopleDataUpdated}"></persona-main-dm>

  `;
  }

  peopleDataUpdated(e){
      console.log("Se han recogido las personas del backend");
      this.people = e.detail.people
  }

  personFormStore(e) {
    console.log("personFormStore");
    console.log(e.detail)

    if (e.detail.editingPerson === true) {
      console.log("Se va a actualizar la persona de nombre " + e.detail.person.name)

      this.people = this.people.map(
        person => person.name === e.detail.person.name ? e.detail.person : person
      )

    } else {
      console.log("Se va a almacenar una persona nueva")
      this.people = [...this.people, e.detail.person]
    }

    this.showPersonForm = false
  }
  personFormClose() {
    console.log("personFormClose");
    this.showPersonForm = false
  }

  showPersonFormData() {
    this.shadowRoot.getElementById("peopleList").classList.add("d-none")
    this.shadowRoot.getElementById("personForm").classList.remove("d-none")
  }

  showPersonList() {
    this.shadowRoot.getElementById("peopleList").classList.remove("d-none")
    this.shadowRoot.getElementById("personForm").classList.add("d-none")
  }

  deletePerson(e) {
    console.log("deletePerson en Persona-main")
    console.log("Se va a borrar la persona de nombre " + e.detail.name)

    this.people = this.people.filter(
      person => person.name != e.detail.name
    )

    console.log(this.people)
  }

  infoPerson(e) {
    console.log("infoPerson");
    console.log("Se ha pedido más información de la persona de nombre " + e.detail.name);

    let chosenPerson = this.people.filter(
      person => person.name === e.detail.name
    )

    console.log(chosenPerson)

    this.shadowRoot.getElementById("personForm").person = chosenPerson[0]
    this.shadowRoot.getElementById("personForm").editingPerson = true
    this.showPersonForm = true;
  }
}

customElements.define('persona-main', PersonaMain)
